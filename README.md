# dva-example-user-dashboard

详见[《12 步 30 分钟，完成用户管理的 CURD 应用 (react+dva+antd)》](https://github.com/sorrycc/blog/issues/18)。

---

<p align="center">
  <img src="https://zos.alipayobjects.com/rmsportal/bmkNCEoluwGaeGjYjInf.png" />
</p>

## Getting Started
Install dependencies.

```bash
$ npm install
```

Build the Dll
```bash
$ roadhog buildDll
```

Start server.

```bash
$ npm start
```

If success, app will be open in your default browser automatically.

## tips

1. service中的`/api/users/${id}`是通过在.roadhogrc通过proxy配置的。
`/api/users?_page=${page}&_limit=${PAGE_SIZE}`
实际的地址为
http://jsonplaceholder.typicode.com/users?_page=1&_limit=4

2. 在components或者route中，通过connect方法将组建所需要的数值传入，如：

User组件的构造函数

```javascript
function Users({ dispatch, list: dataSource, loading, total, page: current }) {
  ...
}
```

将组件和models连接

```javascript
function mapStateToProps(state) {
  const { list, total, page } = state.users;
  return {
    loading: state.loading.models.users,
    list,
    total,
    page,
  };
}

export default connect(mapStateToProps)(Users);
```

其中我们可以看到dispatch是通过框架直接传入到组件的。 

3. 偏函数
通过bind传入一部分参数：

```javascript
  //函数定义需要两个参数
  function editHandler(id, values) {
    dispatch({
      type: 'users/patch',
      payload: { id, values },
    });
  }

  //在使用时传入一部分参数
  render: (text, record) => (
        <span className={styles.operation}>
          <UserModal record={record} onOk={editHandler.bind(null, record.id)}>
            <a>Edit</a>
          </UserModal>
          <Popconfirm title="Confirm to delete?" onConfirm={deleteHandler.bind(null, record.id)}>
            <a href="">Delete</a>
          </Popconfirm>
        </span>
      )

// 如
function add(x,y,z){return x+y+z;}
var add1 = add.bind(null,1,2);
add1(7);
//结果返回10；

  ```

  4. 直接将对象作为props传递给组件

  ```javascript

  //在/components/User/User.js组件中，render时直接将record作为属性向下传递
  render: (text, record) => (
        <span className={styles.operation}>
          <UserModal record={record} onOk={editHandler.bind(null, record.id)}>
            <a>Edit</a>
          </UserModal>
          <Popconfirm title="Confirm to delete?" onConfirm={deleteHandler.bind(null, record.id)}>
            <a href="">Delete</a>
          </Popconfirm>
        </span>
      ),

  //在/components/User/UserModal.js组件中直接根据record属性取值
  render() {
    const { children } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { name, email, website } = this.props.record;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };

  ```

  5. 不同页面的跳转

```javascript
import React from 'react';
import { Menu, Icon } from 'antd';
import { Link } from 'dva/router';

function Header({ location }) {
  return (
    <Menu
      selectedKeys={[location.pathname]}
      mode="horizontal"
      theme="dark"
    >
      <Menu.Item key="/users">
        <Link to="/users"><Icon type="bars" spin="true" />Users</Link>
      </Menu.Item>
      <Menu.Item key="/">
        <Link to="/"><Icon type="home" />Home</Link>
      </Menu.Item>
      <Menu.Item key="/404">
        <Link to="/page-you-dont-know"><Icon type="frown-circle" />404</Link>
      </Menu.Item>
      <Menu.Item key="/antd">
        <a href="https://github.com/dvajs/dva">dva</a>
      </Menu.Item>
    </Menu>
  );
}

export default Header;
```
6.  属性也可以作为构造函数参数传入

```javascript

// /Components/MainLayout/Header.js
function Header({ location }) {
  ...
}

// /Components/MainLayout/MainLayout.js 创建组件对象,location作为props传入Header的构造函数

function MainLayout({ children, location }) {
  return (
    <div className={styles.normal}>
      <Header location={location} />
      <div className={styles.content}>
        <div className={styles.main}>
          {children}
        </div>
      </div>
    </div>
  );
}
```

7. model需要通过app注册

在进行dispatch时，将会根据已经注册的model寻找对应的方法

```javascript
// 通过app.model(...)
const cached = {};
function registerModel(app, model) {
  if (!cached[model.namespace]) {
    app.model(model);
    cached[model.namespace] = 1;
  }
}

// 在使用实际的route之前需要先注册该route用到的model
{
  path: '/users',
  name: 'UsersPage',
  getComponent(nextState, cb) {
    require.ensure([], (require) => {
      registerModel(app, require('./models/users'));
      cb(null, require('./routes/Users'));
    });
  },
},
```

8. component 和 model

component和model要建立连接，每个component都有自己的state和props，只要state发生变化，就会重新渲染该组件
通常情况下是在model的reducers中更改component的state，只要state有变化，component就会重新渲染
在组件中需要通过connect和状态建立关联，这个状态是所有注册到app.model中的所有model的state，键值是该model的名称
https://jsfiddle.net/andyge/b20dusjc/

```javascript

// 2. Model
app.model({
  namespace: 'count',
  state: 0,
  effects: {
    *add(action, { put, call }) {
      yield call(delay, 1000);
      yield put({ type: 'minus' });
    },
  },
  reducers: {
    add(count) { return count + 1 },
    minus(count) { return count - 1 },
  },
});

// 3. View
const App = connect(({ count }) => ({
  count
}))(function(props) {
  return (
    <div>
      <h2>{ props.count }</h2>
      <button key="add" onClick={() => { props.dispatch({type: 'count/add'})}}>+</button>
      <button key="minus" onClick={() => { props.dispatch({type: 'count/minus'})}}>-</button>
    </div>
  );
});

//上面的connect其实也可以写成
// 3. View
const App = connect((state) => ({
  count:state.count
}))(function(props) {
  return (
    <div>
      <h2>{ props.count }</h2>
      <button key="add" onClick={() => { props.dispatch({type: 'count/add'})}}>+</button>
      <button key="minus" onClick={() => { props.dispatch({type: 'count/minus'})}}>-</button>
    </div>
  );
});

```
