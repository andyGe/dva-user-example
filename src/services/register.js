import Parse from './parse';

export const fetch = async ({ page }) => {
    try {
        var size = 4;
        const query = new Parse.Query('UserInfo');
        const total = await query.count();
        query.descending('createdAt');
        query.skip((page - 1) * size).limit(size);
        let arr = await query.find();
        arr = arr.map((item) => {
            var it = item.toJSON();
            it.id = it.objectId;
            return it;
        });
        const ret = {
            data: arr,
            headers: {},
        };
        ret.headers['x-total-count'] = total;
        return Promise.resolve(ret);
    } catch (e) {
        return Promise.reject('系统繁忙!');
    }
};

export const create = async (user) => {
    try {
        const User = Parse.Object.extend('UserInfo');
        const newItem = new User();
        newItem.set('name', user.name);
        newItem.set('email', user.email);
        newItem.set('website', user.website);
        await newItem.save();
        return Promise.resolve();
    } catch (e) {
        return Promise.reject('系统繁忙!');
    }
};

export const patch = async (id, user) => {
    try {
        const query = new Parse.Query('UserInfo');
        query.equalTo('objectId', id);
        const newItem = await query.first();
        newItem.set('name', user.name);
        newItem.set('email', user.email);
        newItem.set('website', user.website);
        await newItem.save();
        return Promise.resolve();
    } catch (e) {
        return Promise.reject('系统繁忙!');
    }
};

export const remove = async (id) => {
    try {
        const query = new Parse.Query('UserInfo');
        query.equalTo('objectId', id);
        const destroyItem = await query.first();
        await destroyItem.destroy();
        return Promise.resolve();
    } catch (e) {
        return Promise.reject('系统繁忙!');
    }
};
