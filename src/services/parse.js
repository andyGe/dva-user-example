import Parse from 'parse';
//import { config } from '../../utils';
let config = {
  name: 'TODO Demo',
  appId: 'zV9Ja1RnaUYZH5o',
  serverURL: 'http://localhost:1338/api',
};
Parse.initialize(config.appId);
Parse.serverURL = config.serverURL;
Parse.Promise.enableAPlusCompliant();
export default Parse;
